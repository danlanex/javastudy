package CCAT;

class RunnableDemo implements Runnable {
    private Thread t;
    private String threadName;

    RunnableDemo(String name) {
        threadName = name;
        System.out.println("正在创建（Creat） " +  threadName );
    }

    public void run() {
        System.out.println("正在运行（Run） " +  threadName );
        try {
            for(int i = 4; i > 0; i--) {
                System.out.println("线程（Thread）: " + threadName + ", " + i);
                // 让线程睡眠一会
                Thread.sleep(50);
            }
        }catch (InterruptedException e) {
            System.out.println("线程（Thread） " +  threadName + " 阻塞（interrupted）");
        }
        System.out.println("线程（Thread） " +  threadName + " 退出（exiting）");
    }

    public void start () {
        System.out.println("正在开始（Start） " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}

class TestThread {

    public static void main(String args[]) {
        RunnableDemo R1 = new RunnableDemo( "线程（Thread）1");
        R1.start();

        RunnableDemo R2 = new RunnableDemo( "线程（Thread）2");
        R2.start();
    }
}