package shape1;

//图像类群【第一版】的使用实例
public class ShapeTester {

    public static void main(String[] args) {
        //下述声明错误：抽象类无法实例化
//		Shape s = new Shape();

        Shape[] a = new Shape[2];
        a[0] = new Point();				//点
        a[1] = new Rectangle(4, 3);		//长方形

        for (Shape s : a) {
            s.draw();					//绘图
            System.out.println();
        }
/*		for (int i = 0; i < a.length; i++){
			a[i].draw();				//绘图
			System.out.println();
		}*/
    }

}
