package player;
//CD播放器
public class CDPlayer implements Player {

    @Override
    public void play() {
        System.out.println("CD播放开始！");      //播放
    }

    @Override
    public void stop() {
        System.out.println("CD播放结束！");      //停止
    }

    public void cleaning() {
        System.out.println("已清洗磁头。");      //清洗
    }
}
