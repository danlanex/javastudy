package player;
//扩展播放器 接口（带有慢放功能）
public interface ExPlayer extends Player {
    void slow();    //慢放
}
