package player;
//换肤接口
public interface Skinnable {
    //字段为：public static final
    int BLACK = 0;      //黑色
    int RED = 1;        //红色
    int GREEN = 2;      //绿色
    int BLUE = 3;       //蓝色
    int LEOPARD = 4;    //豹纹

    //方法为：public abstract
    void changeSkin(int skin);      //换肤
}
