package player;
//视频播放器
public class VideoPlayer implements Player {
    private int id;     //制造编号
    private static int count = 0;   //到目前为止已经赋予的编号

    public VideoPlayer() {  //构造函数
        id = ++count;
    }

    @Override
    public void play() {
        System.out.println("视频播放开始！");      //播放
    }

    @Override
    public void stop() {
        System.out.println("视频播放结束！");      //停止
    }

    public void printInfo() {
        System.out.println("该机器的制造编号为：" + id);      //显示制造编号
    }
}
