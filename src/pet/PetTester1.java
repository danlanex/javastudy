package pet;
//宠物类的使用示例（验证多态）
public class PetTester1 {
    public static void main(String[] args) {
        Pet blue = new Pet("Blue","淡蓝");
        blue.introduce();
        System.out.println();

        RobotPet siri = new RobotPet("Siri","danlan");
        siri.introduce();
        System.out.println();

        Pet p = siri;
        p.introduce();
    }
}
