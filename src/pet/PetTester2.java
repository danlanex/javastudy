package pet;
//宠物类的使用示例（使用方法的参数来验证多态）
public class PetTester2 {
    //让p引用的实例进行自我介绍
    static void intro(Pet p) {
        p.introduce();
    }

    public static void main(String[] args) {
        Pet[] a = {
                new Pet("Blue","淡蓝"),
                new RobotPet("Siri","danlan"),
                new Pet("宠物","淡蓝")
        };

        for (Pet p : a) {
            intro(p);
            System.out.println();
        }
    }
}
