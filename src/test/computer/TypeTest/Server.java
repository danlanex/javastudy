package test.computer.TypeTest;
//个人计算机类
class Server extends Computers{
    private int type = 2;

    protected Server(String name){
        super(name);
    }

    protected void getInfo(){
        System.out.println("计算机名是：" + getName());
        System.out.println("计算机类型是：" + getType(type));
    }

}
