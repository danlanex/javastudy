package test.computer.TypeTest;
//计算机总类
class Computers {
    private String name;
    private String[] types = {"计算机类型错误","PC-个人计算机","Server-服务器计算机"};

    protected Computers(String name){
        this.name = name;
    }

    protected String getName(){
        return name;
    }

    protected String getType(int x){
        int y = 0;
        for (int i = 0; i < types.length; i++) {
            if (x == i)
                y = i;
        }
        return types[y];
    }
}
