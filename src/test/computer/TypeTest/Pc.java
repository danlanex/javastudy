package test.computer.TypeTest;
//个人计算机类
class Pc extends Computers{
    private int type = 1;

    protected Pc(String name){
        super(name);
    }

    protected void getInfo(){
        System.out.println("计算机名是：" + getName());
        System.out.println("计算机类型是：" + getType(type));
    }

}
