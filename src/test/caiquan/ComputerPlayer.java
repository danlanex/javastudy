package test.caiquan;

import java.util.Random;

//猜拳计算机玩家类（随机出拳）
class ComputerPlayer extends CaiQuanPlayer {
    //使用随机数
    Random rand = new Random();

    //重写思考出拳的方法
    @Override
    void setQuanfa() {
        squanfa = QUANFA[rand.nextInt(2)];
    }

    //实例初始化器，告诉主程序电脑玩家类已生成随机拳法
    {
        System.out.println("电脑玩家已做好准备！");
    }

}
