package test.caiquan;

//猜拳人类玩家类（键盘输入出拳）
class HumanPlayer extends CaiQuanPlayer {
    //定义人类玩家的拳法变量（从主程序获取）
    int q;

    //重写思考出拳方法
    @Override
    void setQuanfa() {
        squanfa = QUANFA[q];
    }

    //实例初始化器，告诉主程序人类玩家类已生成随机拳法
    {
        System.out.println("人类玩家已做好准备！");
    }
}
