package test.caiquan;
//猜拳玩家抽象类
abstract class CaiQuanPlayer {
    static{
        System.out.println("猜拳游戏正在载入...");
        System.out.println("游戏即将开始，请做好准备！");
    }
    
    //潜在拳法
    static final String[] QUANFA = {"石头","剪刀","布"};

    //实际拳法
    String squanfa;

    //思考出拳，人类为输入，计算机为随机
    abstract void setQuanfa();

    //出拳结果
    String getQuanfa() {
        return squanfa;
    }
}