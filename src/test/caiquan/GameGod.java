package test.caiquan;
//游戏规则
class GameGod {

    //出拳对决（判断）
    static String CaiQuanPK(CaiQuanPlayer ai, CaiQuanPlayer man) {
        if (ai.getQuanfa() == "石头") {
            switch (man.getQuanfa()) {
                case "石头":
                    return "平局";
                case "剪刀":
                    return ai.getQuanfa();
                case "布":
                    return man.getQuanfa();
                default:
                    return "错误代码01";
            }
        }else if (ai.getQuanfa() == "剪刀") {
            switch (man.getQuanfa()) {
                case "石头":
                    return man.getQuanfa();
                case "剪刀":
                    return "平局";
                case "布":
                    return ai.getQuanfa();
                default:
                    return "错误代码01";
            }
        }else if (ai.getQuanfa() == "布") {
            switch (man.getQuanfa()) {
                case "石头":
                    return ai.getQuanfa();
                case "剪刀":
                    return man.getQuanfa();
                case "布":
                    return "平局";
                default:
                    return "错误代码01";
            }
        }
        return "错误代码02";
    }
}
