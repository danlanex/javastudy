package test.caiquan;

import java.util.Scanner;

//猜拳游戏主程序
class CaiQuanGame {
    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        //实例化人类玩家和电脑玩家
        CaiQuanPlayer[] player = new CaiQuanPlayer[2];
        player[0] = new ComputerPlayer();
        player[1] = new HumanPlayer();
        HumanPlayer man = (HumanPlayer)player[1];

        for (int i=1; i>0; i++){
            System.out.println("请问要出什么拳？（0为石头，1为剪刀，2为布）");
            int q = stdIn.nextInt();
        if (q == 0 || q == 1 || q == 2){
            //读入人类出拳并传给人类玩家类
            man.q = q;
            break;
        }else
            System.out.println("输入错误，请重新输入！");
        }
        
        //出拳
        player[0].setQuanfa();
        man.setQuanfa();
        
        System.out.println("AI出的是：" + player[0].getQuanfa());
        System.out.println("你出的是：" + player[1].getQuanfa());
        
        //调用出拳对决方法
        if (player[0].getQuanfa() == GameGod.CaiQuanPK(player[0], player[1])) {
            System.out.println("AI取得了胜利！");
        } else if (player[1].getQuanfa() == GameGod.CaiQuanPK(player[0], player[1])) {
            System.out.println("你取得了胜利！");
        } else {
            switch (GameGod.CaiQuanPK(player[0], player[1])) {
                case "平局":
                    System.out.println("平局！"); break;
                case "错误代码01":
                    System.out.println("错误代码01"); break;
                case "错误代码02":
                    System.out.println("错误代码02"); break;
                default:
                   System.out.println("错误代码03"); break;
            }
        }
    }
}
