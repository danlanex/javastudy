package test;

class Baba {
    String name;
    private int num;

    Baba(String name, int num){
        this.name = name;
        this.num = num;
    }

    String getName(){
        return name;
    }

    int getNum(){
        return num;
    }
}

class Erzi extends Baba{
    static String sname;

    Erzi(String name, int num, String sname){
        super(name, num);
        this.sname = sname;
    }
    String getSname(){
        return sname;
    }
}

class run{
    public static void main(String[] args) {
        Erzi son = new Erzi("爸爸", 1000000 ,"儿子");

        System.out.println(son.getName());
        System.out.println(son.getSname());
        System.out.println(son.getNum());
        System.out.println(son.name);
        System.out.println(Erzi.sname);
    }
}