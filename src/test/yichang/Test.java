package test.yichang;

import java.util.Scanner;

//异常测试主程序
public class Test {
    //设计一个将String字符串转换成int整数型的方法，并返回整数型
    //如果转换错误会抛出异常
//    public static int numberFormat(String num) {
//        int x = Integer.parseInt(num);
//        return x;
//    }
    public static ResponseDemo numberFormat(String num) {
        ResponseDemo response = new ResponseDemo();
        try {
            int x = Integer.parseInt(num);
            response.setCode(100);
            response.setMessage("数据转化成功，已将值放入data");
            response.setData(x);
        } catch(Exception e) {
            response.setCode(101);
            response.setMessage("输入的字符串除了数字以外还有的其他类型格式，数据错误！");
        }
        return response;
    }

    //用户端使用程序
    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        System.out.println("你好请输入内容，会转换成整数：");
        String num = stdIn.next();

        //调用方法
//        System.out.println(numberFormat(num));
        ResponseDemo reponse = numberFormat(num);
        if (reponse.getCode() == 101) {
            System.out.println(reponse.getMessage());
        } else {
            System.out.println(reponse.getData());
        }
    }
}
