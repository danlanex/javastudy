package test.yichang;

public class ResponseDemo {
    private int code;   //错误代码，100为正确，101为错误
    private String message; //错误信息
    private int data;         //转换后的数值存放处
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public int getData() {
        return data;
    }
    public void setData(int data) {
        this.data = data;
    }
    @Override
    public String toString() {
        return "ResponseDemo [错误编码为：" + code + "，错误信息为：" + message + "，数据=" + data + "]";
    }

}
