package Chap15;
//空引用和字符串
public class StringTester {
    public static void main(String[] args) {
        String s1 = null;   //空引用“无引用”
        String s2 = "";     //""引用

        System.out.println("字符串s1 = " + s1);
        System.out.println("字符串s2 = " + s2);
    }
}
