package Chap15;
//改变字符串的引用目标
public class ChangeString {
    public static void main(String[] args) {
        String s1 = "ABC";
        String s2 = "XYZ";

        s1 = "XYZ";
        System.out.println("s1" + s1);
        System.out.println("s2" + s2);

        System.out.println();
    }
}
