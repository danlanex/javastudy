package classtest.account3;
//银行账户类【第3版】
public class Account {
    //类变量（静态字段）
    private static int counter = 0;     //赋到了哪一个标识编号

    //实力变量（非静态字段）
    private String name;
    private String no;
    private long balance;
    private int id;

    //构造函数
    public Account(String n, String num, long z){
        name = n;
        no = num;
        balance = z;
        id = ++counter;
    }

    //确认账户名
    public String getName(){
        return name;
    }

    //确认账号
    public String getNo(){
        return no;
    }

    //确认可用余额
    public long getBalance(){
        return balance;
    }

    //获取标识编号
    public int getId(){
        return id;
    }

    //存入金额
    public void deposit(long k){
        balance += k;
    }

    //取出金额
    public void withdraw(long k){
        balance -= k;
    }
}
