package classtest;

class Account{
    String name;
    String no;
    long $;
}

public class AccountsTester {
    public static void main(String[] args) {
        Account a = new Account();
        Account b = new Account();

        a.name = "a";
        a.no = "1";
        a.$ = 1000;

        b.name = "b";
        b.no = "2";
        b.$ = 700;
    }
}
