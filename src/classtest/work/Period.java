package classtest.work;

import classtest.day3.Day;

public class Period {
    private Day from;
    private Day to;

    //构造函数
    public Period(Day from, Day to){
        this.from = new Day(from);
        this.to = new Day(to);
    }

    //方法
    public String getPeriod(){
        return String.format("期间共间隔%d年%d月%d日",Math.abs(from.getYear() - to.getYear()), Math.abs(from.getMonth() - to.getMonth()), Math.abs(from.getDate() - to.getDate()));
    }
}
