package classtest.account2;

class Account{
    private String name;
    private String no;
    private long $;

    //构造函数
    Account(String n, String num, long z){
        name = n;
        no = num;
        $ = z;
    }

    //确认账户名
    String getName(){
        return name;
    }
    //确认账号
    String getNo(){
        return no;
    }

    //确认可用余额
    long get$(){
        return $;
    }

    //存入
    void deposit(long k){
        $ += k;
    }

    //取出
    void withdraw(long k){
        $ += k;
    }
}
public class AccountTester {
    public static void main(String[] args) {
        Account a = new Account("a","1",1000);
        Account b = new Account("b","2",700);
    }
}
