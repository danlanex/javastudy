package classtest.car1;

class Car {
    private String name;    //名称
    private int width;      //宽度
    private int height;     //高度
    private int length;     //长度
    private double x;       //当前位置X坐标
    private double y;       //当前位置Y坐标
    private double fuel;    //剩余燃料

    //--- 构造函数 ---//
    Car(String name, int width, int height, int length, double fuel){
        this.name = name;
        this.width = width;
        this.height = height;
        this.length = length;
        this.fuel = fuel;
        x = y = 0.0;
    }

    double getX(){  //获取X坐标
        return x;
    }
    double getY(){  //获取Y坐标
        return y;
    }
    double getFuel(){   //获取当前剩余燃料
        return fuel;
    }
    //--- 显示汽车信息 ---//
    void putSpec(){
        System.out.println("车名：" + name);
        System.out.println("车宽：" + width);
        System.out.println("车高：" + height);
        System.out.println("车长：" + length);
    }
    //--- 向X方向移动dx、向Y方向移动dy ---//
    boolean move(double dx, double dy){
        double dist = Math.sqrt(dx * dx + dy *dy);  //移动距离
        if (dist > fuel)
            return false;   //无法移动，燃料不足
        else {
            fuel -= dist;   //减掉消耗的燃料
            x += dx;
            y += dy;
            return true;    //移动结束
        }
    }
}
