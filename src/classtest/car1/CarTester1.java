package classtest.car1;

public class CarTester1 {
    public static void main(String[] args) {
        Car Tesla = new Car("特斯拉",1660,1500,3640,40.0);
        Car Apple = new Car("苹果",1660,1525,3695,41.0);

        Tesla.putSpec();
        System.out.println();
        Apple.putSpec();
    }
}
