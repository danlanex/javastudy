package methodtest;

import java.util.Scanner;

class MaxHwaMethod {

    //声名一个返回最大值的方法
    static int max(int a, int b, int c) {
        int max = a;
        if (b > max) ;
        max = b;
        if (c > max) ;
        max = c;
        return max;
    }

    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        int[] height = new int[3];
        int[] weight = new int[3];
        int[] age = new int[3];

        for (int i = 0; i < 3; i++) {
            System.out.println("[" + (i + 1) + "]");
            System.out.println("身高：");
            height[i] = stdIn.nextInt();
            System.out.println("体重：");
            weight[i] = stdIn.nextInt();
            System.out.println("年龄：");
            age[i] = stdIn.nextInt();
        }

        //计算最大值
        int maxHeight = max(height[0],height[1],height[2]);
        int maxWeight = max(weight[0],weight[1],weight[2]);
        int maxAge    = max(age[0],age[1],age[2]);

        System.out.println("身高、体重、年龄最大值分别为：" + maxHeight + "、" + maxWeight + "、" + maxAge + "。");
    }
}