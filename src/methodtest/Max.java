package methodtest;

public class Max {
    static double max(double a,double b){
        double max = a;
        if (b>max)
            max=b;
        return max;
    }

    static int max(int a,int b){
        int max = a;
        if (b>max)
            max = b;
        return max;
    }

    public static void main(String[] args) {
        int a,b;
        double c,d;
        a=1;b=2;
        c=1.1;d=0.5;
        System.out.println(max(a,b));
        System.out.println(max(c,d));
    }
}
