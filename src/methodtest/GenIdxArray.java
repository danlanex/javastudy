package methodtest;

public class GenIdxArray {
    //创建并返回一个全部元素的值与索引相同、元素个数为n的数组
    static int[] idxarray(int n){
        int[] a = new int[n];
        for (int i=0; i<n; i++){
            a[i] = i;
        }
        return a;
    }
}
