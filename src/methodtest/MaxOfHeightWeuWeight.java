package methodtest;

public class MaxOfHeightWeuWeight {
    //计算并返回数组a中的最大值
    static int maxOf(int[] a){
        int max = a[0];
        for (int i = 1;i < a.length;i++){
            if (a[i] > max)
                max = a[i];
        }
        return max;
    }

    public static void main(String[] args) {
        int[] a = {1,2,3,4,5};
        int[] b = new int[5];
        b = new int[] {1,2,3,4,5};

        System.out.println(maxOf(a));
    }
}
