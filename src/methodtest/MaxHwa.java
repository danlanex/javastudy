package methodtest;

import java.util.Scanner;

class MaxHwa {
    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        int[] height = new int[3];
        int[] weight = new int[3];
        int[] age    = new int[3];

        for(int i = 0; i < 3; i++){
            System.out.println("[" + (i + 1) + "]");
            System.out.println("身高：");   height[i] = stdIn.nextInt();
            System.out.println("体重：");   weight[i] = stdIn.nextInt();
            System.out.println("年龄：");   age[i]    = stdIn.nextInt();
        }

        //计算最大值
        int maxHeight = height[0];
        if (height[1] > height[0])  maxHeight = height[1];
        if (height[2] > height[0])  maxHeight = height[2];

        int maxWeight = weight[0];
        if (weight[1] > weight[0])  maxWeight = weight[1];
        if (weight[2] > weight[0])  maxWeight = weight[2];

        int maxAge = age[0];
        if (age[1] > age[0])  maxAge = age[1];
        if (age[2] > age[0])  maxAge = age[2];

        System.out.println("身高、体重、年龄最大值分别为：" + maxHeight + "、" + maxWeight + "、" + maxAge + "。");
    }
}