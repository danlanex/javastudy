package API;
//判断两个值，三个值，数组中最大值最小值的工具类
public final class MinMax {
    //判断两个数的最大值
    static {
        System.out.println("欢迎使用danlan制作的MinMax类（此提示只显示在一次）");
    }
    public static int Max(int a, int b){
        int max = a;
        if (b > max)
            max = b;
        return max;
    }
    //判断两个数的最小值
    public static int Min(int a, int b) {
        int min = a;
        if (b < min)
            min = b;
        return min;
    }
    {
        System.out.println("测试用");
    }
    //判断三个数的最大值
    public static int XMax(int a, int b, int c) {
        int max = a;
        if (b > max)
            max = b;
        if (c > max)
            max = c;
        return max;
    }
    //判断三个数的最小值
    public static int XMin(int a, int b, int c) {
        int min = a;
        if (b < min)
            min = b;
        if (c < min)
            min = c;
        return min;
    }
    //找出数组中的最大值
    public static int AMax(int[] a){
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] > max)
                max = a[i];
        }
        return max;
    }
    //找出数组中的最小值
    public static int AMin(int[] a){
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] < min)
                min = a[i];
        }
        return min;
    }
}
