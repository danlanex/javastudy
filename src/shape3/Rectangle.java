package shape3;
//长方形
public class Rectangle extends Shape implements Plane2D  {
    private int width;      //长
    private int height;     //宽

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int getArea() { return width * height; };

    @Override
    public String toString() {
        return "Rectangle(width:" + width + ", heigth:" + ")";
    }

    @Override
    public void draw() {
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++)
                System.out.print('*');
            System.out.println();
        }
    }
}
