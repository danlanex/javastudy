package Chap10;

class Id {
    static int n = 1;
    private static int counter = 0;
    private int id;

    //返回最后id的类方法
    static int getMaxID(){
        return counter;
    }

    static int getN(){
        return n;
    }

    static void setN(int n){
        Id.n = n;
    }

    //构造函数
    public Id(){
        id = ++ counter + n - 1;
        counter = id;
    }

    //获取标识编号
    public int getId(){
        return id;
    }
}

public class IdTester{
    public static void main(String[] args) {
//        Id.counter = 1000;
        Id a = new Id();
        System.out.println(Id.getMaxID());
        Id b = new Id();
        System.out.println(Id.getMaxID());
        Id c = new Id();
        System.out.println(Id.getMaxID());
        System.out.println("N="+Id.getN());
        Id.setN(4);
        System.out.println("N="+Id.getN());
        Id d = new Id();
        System.out.println(Id.getMaxID());
        Id e = new Id();
        System.out.println(Id.getMaxID());
        Id f = new Id();
        System.out.println(Id.getMaxID());

        System.out.println("a的标识编号：" + a.getId());
        System.out.println("b的标识编号：" + b.getId());

//        System.out.println("Id.counter = " + Id.counter);
//        System.out.println("a.counter = " + a.counter);
//        System.out.println("b.counter = " + b.counter);

    }
}
