package Chap10;

import java.util.Random;

class RandId {
    private static int counter;
    private int id;

    static {
        Random rand = new Random();
        counter = rand.nextInt(10) * 100;
    }

    //构造函数
    public RandId(){
        id = ++counter;
    }

    //获取标识编号
    public int getId(){
        return id;
    }
}

public class RandIdTester{
    public static void main(String[] args) {
        RandId a = new RandId();
        RandId b = new RandId();
        RandId c = new RandId();

        System.out.println("a的标识符编号" + a.getId());
        System.out.println("b的标识符编号" + b.getId());
        System.out.println("c的标识符编号" + c.getId());
    }
}
